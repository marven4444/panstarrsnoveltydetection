def CompareElements(predictions, actual):
    count = 0
    for i in predictions:
        for j in actual:
            if(i == j):
                count += 1
    return count

def ExtractFileNames(filepaths):
    filenames =[]
    for i in filepaths:
        splitStr = i.split('/')
        for j in splitStr:
            found = False;
            for k in j:
                if(k.isdigit() and found == False):
                    filenames.append(j)
                    found = True
    return filenames

def ReadFile(filename):
    filepaths = []
    f = open(filename, "r")
    for i in f:
        i = i.rstrip()
        filepaths.append(i)
    return filepaths
        
                    
        
pred = ["abc", "b", "c", "d"]
actual = ["z", "f", "e", "abc"]
filenames = ExtractFileNames(filepaths)
print(filenames)
#print(CompareElements(pred, actual))