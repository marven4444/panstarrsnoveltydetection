from zipfile import ZipFile
import keras as k
import os
import tensorflow as tf
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D, Flatten
from keras.models import Model
from keras import backend as K
import numpy as np
import keras.backend as kb
from keras.backend import squeeze
import matplotlib.pyplot as plt
from keras.callbacks import TensorBoard


x_train = []
x_validate = []
x_test = []
x_train_to_test = []

# specifying the zip file name 
file_name = "CircularEllipticalAutoencoder.zip"
  
# opening the zip file in READ mode 
with ZipFile(file_name, 'r') as zip: 
    # printing all the contents of the zip file 
    zip.printdir() 
  
    # extracting all the files 
    print('Extracting all the files now...') 
    zip.extractall() 
    print('Done!') 

# specifying the zip file name 
file_name = "CircularEllipticalAutoencoderTesting.zip"
  
# opening the zip file in READ mode 
with ZipFile(file_name, 'r') as zip: 
    # printing all the contents of the zip file 
    zip.printdir() 
  
    # extracting all the files 
    print('Extracting all the files now...') 
    zip.extractall() 
    print('Done!')     

data_dir = os.path.join(os.getcwd(), "CircularEllipticalAutoencoderTesting")
datagen_kwargs = dict(rescale=1./255, validation_split=.20)
dataflow_kwargs = dict(target_size=(120, 120), batch_size=1,
                   interpolation="bilinear")

test_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
    **datagen_kwargs)
test_generator = test_datagen.flow_from_directory(
    data_dir, shuffle=False, **dataflow_kwargs)

for i in range(99):
  y, z = next(test_generator)
  x_test.append(y)         

print(len(x_test))
#x_test = np.squeeze(x_test)
print(x_test[0].shape)
#from PIL import Image

#optimizer = tf.keras.optimizers.Adam(0.001)
#model.compile(optimizer= optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
data_dir = os.path.join(os.getcwd(), "CircularEllipticalAutoencoder")
datagen_kwargs = dict(rescale=1./255, validation_split=.20)
dataflow_kwargs = dict(target_size=(120, 120), batch_size=1,
                   interpolation="bilinear")

valid_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
    **datagen_kwargs)
valid_generator = valid_datagen.flow_from_directory(
    data_dir, subset="validation", shuffle=False, **dataflow_kwargs)

train_datagen = valid_datagen
train_generator = train_datagen.flow_from_directory(
    data_dir, subset="training", shuffle=True, **dataflow_kwargs)



def show_batch(image_batch):
  plt.figure(figsize=(10,10))
  for n in range(20):
      ax = plt.subplot(5,5,n+1)
      plt.imshow(image_batch[n])
      plt.axis('off')
  plt.show()      

for i in range(160):
  y, z = next(train_generator)
  x_train.append(y)

for i in range(20):
  y, z = next(valid_generator)
  x_validate.append(y)

for i in range(161, 180):
    y, z = next(train_generator)
    x_train_to_test.append(y)

#show_batch(x_test)


x_train = np.array(x_train, dtype=np.float32)
x_validate = np.array(x_validate, dtype=np.float32)
x_test = np.array(x_test, dtype=np.float32)
x_train_to_test = np.array(x_train_to_test, dtype=np.float32)




#input_img = Input(shape=(43200, 1, 1))

# x = Conv2D(2, (7, 7), activation='relu', padding='same')(input_img)
# x = MaxPooling2D((2, 2), padding='same')(x)
# x = Conv2D(4, (2, 2), activation='relu', padding='same')(x)
# x = MaxPooling2D((2, 2), padding='same')(x)
# x = Conv2D(16, (2, 2), activation='relu', padding='same')(x)
# encoded = x
# x = Conv2D(8, (2, 2), activation='relu', padding='same')(encoded)
# # x = MaxPooling2D((2, 2), padding='same')(x)
# x = UpSampling2D((2, 2))(x)
# x = Conv2D(8, (2, 2), activation='relu', padding='same')(x)
# x = UpSampling2D((2, 2))(x)
# x = Conv2D(4, (2, 2), activation='relu', padding='same')(x)
# x = UpSampling2D((2, 2))(x)
# x = MaxPooling2D((4, 4), padding='same')(x)
# decoded = Conv2D(1, (2, 2), activation='softmax', padding='same')(x)
# decoded = Flatten()(decoded)
# decoded = k.layers.Reshape((-1, 43200, 1, 1))(decoded)

input_img = Input(shape=(120, 120, 3))
encoded = Dense(128, activation='relu')(input_img)
encoded = Dense(64, activation='relu')(encoded)
encoded = Dense(32, activation='relu')(encoded)

decoded = Dense(64, activation='relu')(encoded)
decoded = Dense(128, activation='relu')(decoded)
decoded = Dense(3, activation='sigmoid')(decoded)

# this model maps an input to its reconstruction
autoencoder = Model(input_img, decoded)


autoencoder.compile(optimizer='RMSProp', loss='binary_crossentropy', metrics=['accuracy'])

x_train = np.squeeze(x_train)
print(x_train[0].shape)

x_validate = np.squeeze(x_validate)
print(x_validate[0].shape)




history = model.fit(X_train, Y_train_flattened)


history = autoencoder.fit(x_train, x_train,
                epochs=5,
                validation_data=(x_validate, x_validate),
                batch_size=10,                
                shuffle=True)                          
                #callbacks=[TensorBoard(log_dir='/tmp/autoencoder')])
    

#print(len(x_test))
#x_test.append(x_train_to_test)
count = 0    
lossVals = []
for i in range(0, len(x_train_to_test)):
    count = count + 1
    test_results = autoencoder.evaluate(x_train_to_test[i], x_train_to_test[i])
    print(test_results)
    lossVals.append(test_results[0])
thirtyPercentile = np.percentile(lossVals, 30)    
fortyPercentile = np.percentile(lossVals, 40)    
fiftyPercentile = np.percentile(lossVals, 50)
sixtyPercentile = np.percentile(lossVals, 60)
seventyPercentile = np.percentile(lossVals, 70)
eightyPercentile = np.percentile(lossVals, 80)
nintyPercentile = np.percentile(lossVals, 90)
hundredPercentile = np.percentile(lossVals, 100)

print(fortyPercentile)
print(fiftyPercentile)
print(sixtyPercentile)
print(seventyPercentile)
print(eightyPercentile)
print(nintyPercentile)
print(hundredPercentile)
    
print('done')
countThirtyP = 0
countFortyP = 0
countFiftyP = 0
countSixtyP = 0
countSeventyP = 0
countEightyP = 0
countNintyP = 0
countHundredP = 0

for i in range(0, len(x_test)):
    count = count + 1
    test_results = autoencoder.evaluate(x_test[i], x_test[i])
    print(autoencoder.metrics_names)
    print(test_results)
    #if(test_results[0] > hundredPercentile):
     #   countHundredP = countHundredP + 1
    if(test_results[0] > nintyPercentile):
        countNintyP = countNintyP + 1
    elif(test_results[0] > eightyPercentile):
        countEightyP = countEightyP + 1
    elif(test_results[0] > seventyPercentile):
        countSeventyP = countSeventyP + 1
    elif(test_results[0] > sixtyPercentile):
        countSixtyP = countSixtyP + 1
    elif(test_results[0] > fiftyPercentile):
        countFiftyP = countFiftyP + 1
    elif(test_results[0] > fortyPercentile):
        countFortyP = countFortyP + 1
    elif(test_results[0] > thirtyPercentile):
        countThirtyP = countThirtyP + 1
print(countThirtyP)
print(countFortyP)
print(countFiftyP)
print(countSixtyP)
print(countSeventyP)
print(countEightyP)
print(countNintyP)
#print(countHundredP)     
    

plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

# Plot training & validation loss values
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()
