import pandas as pd
import numpy as np
from sklearn.neighbors import KDTree, KNeighborsClassifier
from sklearn import preprocessing, metrics, svm
import os
import matplotlib.pyplot as plt
import math
import statistics
import matplotlib.font_manager

#from sklearn.cross_validation import test_train_split

print("started")
count = 0

# Read all values in the CSV and create a dataframe
for i in range(1, 15):
    dataframe = pd.read_csv('c:\\Users\\margs\\Documents\\galaxies\\home\\lshamir\\PanStarrs\\images\\MultipleDatasets\\EllipticalCircular\\EllipticalCircular'+str(i)+'.csv')
    #dataframe = pd.read_csv('/homes/marven/TestParsedFile.csv')

    print("read file")
    # Remove the filename column from the dataframe

    print("started")
    #del dataframe['filename']

    #y = dataframe2.shape
    # Drop all rows that are nans
    dataframe = dataframe.dropna()

    target = dataframe.iloc[:, -1]

    dataframe = dataframe.iloc[:, :-1]

    dataframe = dataframe.iloc[:, :]

    X = dataframe.iloc[:, 1:].values

    # Create a min-max normalization scaler
    min_max_scaler = preprocessing.MinMaxScaler()

    # Normalize the values in the dataframe
    x_scaled = min_max_scaler.fit_transform(X)

    x_train = x_scaled[:, :]
    y_train = target[:]


    #Implementation for KNN
    print(x_train.shape)
    print(y_train.shape)

    clf = svm.OneClassSVM(nu=0.4, kernel="rbf", gamma=0.1)
    x_pred = clf.fit_predict(x_train)

    for i in x_pred:
        if(i == 1):
            count = count + 1

    print(count)        
    print(x_pred)
    #print(y_pred)


