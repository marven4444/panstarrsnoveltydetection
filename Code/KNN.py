import pandas as pd
import numpy as np
from sklearn.neighbors import KDTree, KNeighborsClassifier
from sklearn import preprocessing, metrics
import os
import matplotlib.pyplot as plt
import math
import statistics
#from sklearn.cross_validation import test_train_split

print("started")

all_scores_list = []

# Read all values in the CSV and create a dataframe
for i in range(1, 16):
    dataframe = pd.read_csv('c:\\Users\\margs\\Documents\\galaxies\\home\\lshamir\\PanStarrs\\images\\MultipleDatasets\\SpiralCircular\\SpiralCircular'+str(i)+'.csv')
    #dataframe = pd.read_csv('/homes/marven/TestParsedFile.csv')

    print("read file")
    # Remove the filename column from the dataframe

    print("started")
    #del dataframe['filename']



    # Drop all rows that are nans
    dataframe = dataframe.dropna()

    target = dataframe.iloc[:, -1]

    dataframe = dataframe.iloc[:, :-1]

    dataframe = dataframe.iloc[:, :]


    X = dataframe.iloc[:, 1:].values

    # Create a min-max normalization scaler
    min_max_scaler = preprocessing.MinMaxScaler()

    # Normalize the values in the dataframe
    x_scaled = min_max_scaler.fit_transform(X)

    x_train = x_scaled[:177, :]
    x_test = x_scaled[177:, :]
    y_train = target[:177]
    y_test = target[177:]

    #Implementation for KNN
    print(x_train.shape)
    print(x_test.shape)
    print(y_train.shape)
    print(y_test.shape)

    k_range = range(1,10)
    scores = {}
    scores_list = []
    for k in k_range:
        knn = KNeighborsClassifier(n_neighbors = k)
        knn.fit(x_train, y_train)
        y_pred = knn.predict(x_test)
        scores[k] = metrics.accuracy_score(y_test, y_pred)
        scores_list.append(scores[k])
        all_scores_list.append(scores_list)

print(scores) 
#print(scores_list)
#print(all_scores_list)
all_scores_list = np.array(all_scores_list)
res = np.sum(all_scores_list, 0) 
print(res)