import pandas as pd
import numpy as np
from sklearn.neighbors import KDTree
from sklearn import preprocessing
import os
import matplotlib.pyplot as plt
import math
import statistics

print("started")
# Read all values in the CSV and create a dataframe
# Add csv file path to the filename variable 
filename = ''
dataframe = pd.read_csv(filename)

print("read file")

# Drop all rows that are nans
dataframe = dataframe.dropna()

galaxyNames = []
for i, v in enumerate(dataframe.columns):
    if(i == 0):
        galaxyNames.append(dataframe.iloc[:, i].values)
        continue
        tenth_percentile = np.percentile(dataframe.iloc[:, i].values, 10)
        nintieth_percentile = np.percentile(dataframe.iloc[:, i].values, 90)
        tenth_percentile = float(tenth_percentile)
        nintieth_percentile = float(nintieth_percentile)
        dataframe.iloc[:, i] = np.where(dataframe.iloc[:, i].values > tenth_percentile, dataframe.iloc[:, i].values, 0)
        dataframe.iloc[:, i] = np.where(dataframe.iloc[:, i].values < nintieth_percentile, dataframe.iloc[:, i].values, 0)
        print((dataframe.iloc[:, i].values))        


print("computed percentiles")    


X = dataframe.iloc[:, 1:].values

# Create a min-max normalization scaler
min_max_scaler = preprocessing.MinMaxScaler()

# Normalize the values in the dataframe
x_scaled = min_max_scaler.fit_transform(X)

# Implementation for Entropy

# Create a dataframe with the normalized values
df = pd.DataFrame(x_scaled)
X = df.iloc[:, :].values

# Apply the column names of the original dataframe to the new dataframe
col_names = []
for i in dataframe.columns:
    col_names.append(i)
    #print(i)
    

df.columns = col_names[1:]

df[col_names[0]] = galaxyNames[0]

# Assume a bin size    
bin_size = 10

# Array that holds normalized values of all bins
all_norm_percent_values = []

for i in range(0, len(df.columns)-1): 
    # Grab values of a column and plot the histogram
    data = df.iloc[:, i].values 
    
    #print(df.columns[i])
    counts, bins, patches = plt.hist(data, bins = bin_size)

    # Compute the fractional value of the value of the bin in relation to the total number of values
    num_counts = np.sum(counts)    
    percent_values = [i/num_counts for i in counts] 
    percent_values = np.asarray(percent_values)
    percent_values = percent_values.reshape(-1, 1)
    
    
    # Normalize the fractional values
    norm_percent_values = min_max_scaler.fit_transform(percent_values)

    # Append the normalized fractional values to a list to compute entropy
    all_norm_percent_values.append(norm_percent_values)        
    
print("computing entropy")
# Function that computes Entropy.
def Compute_Entropy(values):
    entropy = 0.
    for i, v in enumerate(values):
        if(v > 0):  
            entropy -= v * math.log(v, 2)
    return entropy

print("Computing Standard Deviation")
def ComputeStandardDeviation(values):
    values = [j for i in values for j in i]
    stdDev = statistics.stdev(values)
    return stdDev

all_entropies = []
all_StdDev = []
# Iterate over all of the normalized fractional values of the bins and Compute Entropy for each feature(column)
for i, v in enumerate(all_norm_percent_values):
    entropy = Compute_Entropy(all_norm_percent_values[i])
    stdDev = ComputeStandardDeviation(all_norm_percent_values[i])    
    all_entropies.append(entropy)
    all_StdDev.append(stdDev)
#print(all_StdDev)  
#print(all_entropies)

# Compute entropy/stdDev for each of the values
# The length of all_StdDev is considered but len(all_stdDev) == len(all_entropies). So len(all_entropies) can also be used.
entOverStdDev = []
for i in range(len(all_StdDev)):
    if(all_StdDev[i] == 0):        
        entOverStdDev.append(0)
    else:
        entOverStdDev.append(all_entropies[i]/all_StdDev[i])        

sixtieth_percentile = np.percentile(all_entropies, 60) 
entOverStdDev_percentile = np.percentile(entOverStdDev, 60)

print("Entropy over StdDev: {}".format(entOverStdDev_percentile))
print("fifteenth percentile:{}".format(sixtieth_percentile))

percentileEntropy = np.where(all_entropies < sixtieth_percentile, all_entropies, 0)
percentileEntOverStdDev = np.where(all_StdDev < entOverStdDev_percentile, all_StdDev, 0)

percentileEntropy = percentileEntropy.flatten()
#print(percentileEntropy)

percentileEntOverStdDev = percentileEntOverStdDev.flatten()
#print(percentileEntOverStdDev)

from scipy.spatial.distance import minkowski


Y = df.iloc[:, :-1].values
Z = df.iloc[:, -1].values


num_of_values = {}

num_of_values_2 = {}


for i, v in enumerate(Y):
    num_of_values[i] = v[0]

# for i, v in enumerate(dataframe.values):
#     num_of_values_2[i]= v[0]
    

from itertools import combinations

val = combinations(num_of_values.keys(),2)
    
minkowski_distances = {}   
minimas = {}
maximas = {}
minkowski_keys = {}

for u, v in val:  
    if(u in minimas.keys()):
        minimas[u] = min(minimas[u], minkowski(Y[u], Y[v], 2, percentileEntOverStdDev))
        maximas[u] = max(maximas[u], minkowski(Y[u], Y[v], 2, percentileEntOverStdDev))
    else:
        print(Y[u])
        print(Y[v])
        print(u)
        print(v)
        minimas[u] = minkowski(Y[u], Y[v], 2, percentileEntOverStdDev)
        maximas[u] = minkowski(Y[u], Y[v], 2, percentileEntOverStdDev)
    minkowski_distances[(u, v)] = minkowski(Y[u], Y[v], 2, percentileEntOverStdDev)
    if(u in minkowski_keys.keys()):
        minkowski_keys[u].append(minkowski(Y[u], Y[v], 2, percentileEntOverStdDev))
    else:        
        minkowski_keys[u] = [minkowski(Y[u], Y[v], 2, percentileEntOverStdDev)]
    


print("minimas")    
for index, distance in sorted(minimas.items(), key = lambda x: x[1]):
	print("{} - {} - {}".format(index, Z[index], distance))
    
print("*****")    
    
for index, distance in sorted(minimas.items(), key = lambda x: x[1]):
	print("{} - {}".format(num_of_values_2[index], distance))    



print("******")
print("maximas")
for index, distance in sorted(maximas.items(), key = lambda x: x[1]):
	print("{} - {}".format(Z[index], distance))
    
print("******")    
    
for index, distance in sorted(maximas.items(), key = lambda x: x[1]):
	print("{} - {}".format(num_of_values_2[index], distance))    

print("******")
print("minkowski distances")
for i, v in minkowski_distances.items():
    print("{} - {} - {} - {}".format(i, v, Z[i[0]], Z[i[1]]))

print("******")

for i in df.columns:
    print(i)
    
print("*******")            
K = 5
sorted_minkowski_keys = sorted(minkowski_keys.items(), key=lambda x: x[1])
Knpvals = []
indices = []
for i, v in minkowski_keys.items(): 
    if(len(v) > K):
        vals = minkowski_keys.get(i)
        npvals = np.array(vals)
        Knpval = np.partition(npvals, K)[4]
        Knpvals.append((i, Knpval))
        #print(str(Z[i]) + " " + str(np.partition(npvals, K)[4]))
#sortednpvals = sorted(sortednpvals.items(), key=lambda x: x[1], reverse=True)

sortednpvals = []
for i, v in minkowski_distances.items():    
    for j in range(0, len(Knpvals)):
        if(Knpvals[j][1] == v):
            sortednpvals.append((i, v))

sortednpvals = sorted(sortednpvals, key=lambda x: x[1])            
for i, v in sortednpvals:
    print(Z[i[0]], Z[i[1]], v)