import pandas as pd
import numpy as np
from sklearn.neighbors import KDTree, KNeighborsClassifier, LocalOutlierFactor
from sklearn import preprocessing, metrics
import os
import matplotlib.pyplot as plt
import math
import statistics



print("started")
# Read all values in the CSV and create a dataframe
count = 0
for i in range(1, 10):
    dataframe = pd.read_csv('c:\\Users\\margs\\Documents\\galaxies\\home\\lshamir\\PanStarrs\\images\\MultipleDatasets\\CircularElliptical\\CircularElliptical' + str(i) + '.csv')  

    print("read file")
    # Remove the filename column from the dataframe

    print("started")


    # Drop all rows that are nans
    dataframe = dataframe.dropna()

    target = dataframe.iloc[:, -1]

    dataframe = dataframe.iloc[:, :-1]

    dataframe = dataframe.iloc[:, :]


    X = dataframe.iloc[:, 1:].values

    # Create a min-max normalization scaler
    min_max_scaler = preprocessing.MinMaxScaler()

    # Normalize the values in the dataframe
    x_scaled = min_max_scaler.fit_transform(X)

    x_train = x_scaled[:, :]
    y_train = target[:]

    clf = LocalOutlierFactor(n_neighbors=5)
    print(clf.fit_predict(x_scaled))

    predictions = clf.negative_outlier_factor_


    for i in predictions:
        print(i)
        if(i < -2):
            count = count + 1

print(count)        
